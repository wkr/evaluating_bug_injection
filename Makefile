.PHONY: all clean figures


FIGURES = \
	figures/stats-ranking-sqliteB.pdf \
	figures/stats-ranking-sqliteB-alt.pdf \
	figures/percent_bugs_found.pdf \
	figures/lava-bug-diagram-252446-wide.pdf \
	figures/single-bug-boxplot-tcpdumpB-252466.pdf \
	figures/block-coverage-venn-diagram.pdf \
	figures/cfg-tcpdumpB-etheraddr-string.pdf \
	figures/median-relative-bug-discovery.pdf \
	figures/experiment-setup.pdf \
    figures/distance-to-main-path-cdf-v6x3.pdf \
	figures/partial-callgraph-fileB3-01.pdf \
	figures/partial-callgraph-fileB3-02.pdf \
	figures/by-sa.pdf


%.pdf: %.svg
	rsvg-convert -f pdf -o $@ $<


all: paper.pdf


figures: $(FIGURES)


paper.pdf: paper.tex bib/*.bib figures
	latexmk -xelatex -latexoption=--shell-escape paper.tex

asiafp253.zip: paper.tex bib/extracted.bib Makefile acmart.cls $(FIGURES)
	zip $@ $^

clean:
	latexmk -C paper.tex
	rm -f $(FIGURES)
